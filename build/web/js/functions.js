
$('#last_date').datepicker({
    minDate:new Date()
});
$('#live_date').datepicker({
    minDate:new Date()
});

$('.datep').datepicker({
    changeMonth:true,
    changeYear:true
});

$('.delete').on('click',function(){
    var tid = $(this).attr('data-id');
    $.ajax({
        type: 'POST',
        url: "deleteTender",
        data:{tid:tid},
        success: function (data) {
            if(data == 'true')
            {
                alert('Tender succesfully deleted');
                window.location.href= "view_tender.jsp";
            }
            else
            {
                alert('Failed to delete tender!');
                window.location.href = "view_tender.jsp";
            }
        }
    });
});

$('#reg-company').on('click',function(){
   $('#company-reg').removeClass('hide');
   $('#reg-supplier').removeClass('hide');
   $('#supplier-reg').addClass('hide');
   $('#reg-company').addClass('hide');
   $('#reg-header').html('Register an Account as Banker');
});

$('#reg-supplier').on('click',function(){
   $('#supplier-reg').removeClass('hide');
   $('#reg-company').removeClass('hide');
   $('#company-reg').addClass('hide');
   $('#reg-supplier').addClass('hide');
   $('#reg-header').html('Register an Account as Supplier');
});

$('.bdelete').on('click',function(){
    var bid = $(this).attr('data-id');
    $.ajax({
        type: 'POST',
        url: "Delete_bid",
        data:{bid:bid},
        success: function (data) {
            if(data == 'true')
            {
                alert('Bid succesfully deleted');
                window.location.href= "view_supplier_bids.jsp";
            }
            else
            {
                alert('Failed to delete bid!');
                window.location.href = "view_supplier_bids.jsp";
            }
        }
    });
});

$('.accept').on('click',function(){
    var bid = $(this).attr('data-id');
    var tid = $(this).attr('data-tid');
    $.ajax({
        type: 'POST',
        url: "acceptBid",
        data:{bid:bid,tid:tid},
        success: function (data) {
            if(data == 'true')
            {
                alert('Bid Accepted');
                window.location.href= "view_bids_bank.jsp";
            }
            else
            {
                alert('Failed to accept bid!');
                window.location.href = "view_bids_bank.jsp";
            }
        }
    });
});

$('.reject').on('click',function(){
    var bid = $(this).attr('data-id');
    $.ajax({
        type: 'POST',
        url: "rejectBid",
        data:{bid:bid},
        success: function (data) {
            if(data == 'true')
            {
                alert('Bid rejected!');
                window.location.href= "view_bids_bank.jsp";
            }
            else
            {
                alert('Failed to reject bid!');
                window.location.href = "view_bids_bank.jsp";
            }
        }
    });
});

$('.uaccept').on('click',function(){
    var uid = $(this).attr('data-id');
    $.ajax({
        type: 'POST',
        url: "acceptUser",
        data:{uid:uid},
        success: function (data) {
            if(data == 'true')
            {
                alert('User Accepted');
                location.reload();
            }
            else
            {
                alert('Failed to accept user!');
                location.reload();
            }
        }
    });
});

$('.ureject').on('click',function(){
    var uid = $(this).attr('data-id');
    $.ajax({
        type: 'POST',
        url: "rejectUser",
        data:{uid:uid},
        success: function (data) {
            if(data == 'true')
            {
                alert('User rejected!');
                location.reload();
            }
            else
            {
                alert('Failed to reject user!');
                location.reload();
            }
        }
    });
});

$('#suspend').on('click',function(){
    var id = $(this).attr('data-id');
    $.ajax({
        type: 'POST',
        url: "suspendTender",
        data:{id:id},
        success: function (data) {
            if(data == 'true')
            {
                alert('Tender Suspended!');
                location.reload();
            }
            else
            {
                alert('Failed to suspend tender!');
                location.reload();
            }
        }
    });
});

$('#active').on('click',function(){
    var id = $(this).attr('data-id');
    $.ajax({
        type: 'POST',
        url: "activeTender",
        data:{id:id},
        success: function (data) {
            if(data == 'true')
            {
                alert('Tender Activated!');
                location.reload();
            }
            else
            {
                alert('Failed to activate tender!');
                location.reload();
            }
        }
    });
});

$('.ndelete').on('click',function(){
    var id = $(this).attr('data-id');
    $.ajax({
        type: 'POST',
        url: "deleteNotice",
        data:{id:id},
        success: function (data) {
            if(data == 'true')
            {
                alert('Notice deleted!');
                location.reload();
            }
            else
            {
                alert('Failed to delete notice!');
                location.reload();
            }
        }
    });
});

$('.plan').on('click', function(){
   var plan_id = $(this).attr('data-id');
   var user_id = $(this).attr('data-uid');
   var cost = $(this).attr("data-cost");
   alert(plan_id+" "+user_id+" "+cost);
   $.ajax({
        type: 'POST',
        url: "addOrder",
        data:{plan_id:plan_id,user_id:user_id,cost:cost},
        success: function (data, textStatus, jqXHR) {
            //alert(data);
            if(data=='true')
            {
                location.href = 'payment.jsp';
            }else{
                alert("error");
            }
        }
   });
});

$('#expiry').datepicker();

$('.liveaccept').on('click', function(){
   var id = $(this).attr('data-id');
   var tid = $(this).attr('data-tid');
   var stat = 1;
   $.ajax({
        type: 'POST',
        url: "statusLive",
        data:{id:id,stat:stat,tid:tid},
        success: function (data, textStatus, jqXHR) {
            if(data=='true')
            {
                alert('Bid Accepted');
                location.reload();
            }else{
                alert("error");
            }
        }
   });
});

$('.livereject').on('click', function(){
   var id = $(this).attr('data-id');
   var tid =0;
   var stat = 2;
   $.ajax({
        type: 'POST',
        url: "statusLive",
        data:{id:id,stat:stat,tid:tid},
        success: function (data, textStatus, jqXHR) {
            if(data=='true')
            {
                alert('Bid Rejected');
                location.reload();
            }else{
                alert("error");
            }
        }
   });
});

function checkpass()
{
    var pass = $('#password').val();
    var cpass = $('#cpassword').val();
    var mob = $('#phone').val();
    var out;
    if(/^[0-9]{10}$/.test(mob) == false){
        alert("Enter a valid mobile number!");
        out = false;
    }else if(pass == cpass){
        out = true;
    }else{
        alert('Password Mismatch!');
        out = false;
    }
    return out;
}

function checksupp()
{
    var pass = $('#spassword').val();
    var cpass = $('#scpassword').val();
    var mob = $('#sphone').val();
    var out;
    if(/^[0-9]{10}$/.test(mob) == false){
        alert("Enter a valid mobile number!");
        out = false;
    }else if(pass == cpass){
        out = true;
    }else{
        alert('Password Mismatch!');
        out = false;
    }
    return out;
}

function save($name)
{
   var tname = $('#tname').val().split(',');
   var cat = $('#category').val().split(',');
   var live = $('#live').val().split(',');
   var post = $('#post').val().split(',');
   var last = $('#last').val().split(',');
   var status = $('#status').val().split(',');
   
   var col = ["Sl No","Tender","Category","Post Date","live Date","Last Date","Status"];
   var row=[];
   
   for(var i=0;i<tname.length-1;i++)
   {
       row.push([i+1,tname[i],cat[i],post[i],live[i],last[i],status[i]]);
   }
    var doc = new jsPDF();
    doc.autoTable(col, row, {
    margin: {top: 40},
    addPageContent: function(data) {
    	doc.text($name, 40, 30);
    }
    });
    doc.save($name+'.pdf');
}