<%@page import="model.live"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="includes/header.jsp" flush="true"></jsp:include>

<div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#" title="Post">Home</a></li>
                        <li class="active">Live Tender</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

<div class="content-wrapper">
    <div class="container">
      <div class="row card-body">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <form name="live_tender" action="addLive" method="post">
                <div class="col-sm-4 form-group">
                    <input type="text" name="name" id="name" required="" class="form-control" placeholder="Enter Tender Name">
                </div>
                <div class="col-sm-4 form-group">
                    <input type="text" name="live_date" id="live_date" required="" class="form-control" placeholder="Enter Live Date">
                </div>
                <div class="col-sm-12 form-group">
                    <textarea name="details" id="details" rows="5" required="" class="form-control" placeholder="Enter Details"></textarea>
                </div>
                <button class="btn btn-success" id="live-btn">Add</button>
                </form>
            </div>
            <br>
            <table class="table table-bordered">
                <thead>
                    <th>Sl No</th>
                    <th>Tender Name</th>
                    <th>Live Date</th>
                    <th>Details</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    <%
                    live l = new live();
                    List<live> list = l.getTender();
                    Iterator itr = list.iterator();
                    int i=0;
                    while(itr.hasNext())
                    {
                        l = (live)itr.next();
                        i++;
                    %>
                    <tr><td><%=i%></td>
                        <td><%=l.getLiveName() %></td>
                        <td><%=l.getLiveDate() %></td>
                        <td><%=l.getLiveDetails() %></td>
                        <td><% if(l.getLtStatus()==0)
                            {%>
                            <span class="btn-sm btn-warning">To Bid</span>
                        <%  }else{
                            %>
                                <span class="btn-sm btn-success">Tender Given</span>
                            <%
                            }
                            %>
                        </td>
                    </tr>
                    <%
                    }
                    %>
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>
 <%}%>