/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mysql.jdbc.Statement;
import connection.database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.tomcat.util.codec.binary.Base64;

/**
 *
 * @author Bozz
 */
public class Users {
    int out;
    String output;
    database db = new database();
    private String email,owner_name,company_name,regno,address,password,phone;
    private int user_id,status,usertype,payment;
    
    public int getUserId()
    {
        return user_id;
    }
    public String getEmail()
    {
        return email;
    }
    public String getOwnerName()
    {
        return owner_name;
    }
    public String getCompany()
    {
        return company_name;
    }
    public String getRegno()
    {
        return regno;
    }
    public String getAddress()
    {
        return address;
    }
    public String getPhone()
    {
        return phone;
    }
    public int getUsertype()
    {
        return usertype;
    }
    public int getStatus()
    {
        return status;
    }
    public String getPassword()
    {
        return password;
    }
    public int getPayment()
    {
        return payment;
    }
    
    public void setUserId(int user_id)
    {
        this.user_id=user_id;
    }
    public void setEmail(String email)
    {
        this.email=email;
    }
    public void setOwnerName(String owner_name)
    {
        this.owner_name=owner_name;
    }
    public void setCompany(String company_name)
    {
        this.company_name=company_name;
    }
    public void setRegno(String regno)
    {
        this.regno=regno;
    }
    public void setAddress(String address)
    {
        this.address=address;
    }
    public void setPhone(String phone)
    {
        this.phone=phone;
    }
    public void setUsertype(int usertype)
    {
        this.usertype=usertype;
    }
    public void setStatus(int status)
    {
        this.status=status;
    }
    public void setPassword(String password)
    {
        this.password=password;
    }
    public void setPayment(int payment)
    {
        this.payment=payment;
    }
    
    public int addUser(){
        Connection con = db.dbconnect();
        String s = "insert into users(email,password,owner_name,company_name,regno,address,phone,user_type) values('"+email+"','"+password+"','"+owner_name+"','"+company_name+"','"+regno+"','"+address+"','"+phone+"','"+usertype+"')";
        try {
            PreparedStatement ps =con.prepareStatement(s, Statement.RETURN_GENERATED_KEYS);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            out = rs.getInt(1);
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Users.class.getName()).log(Level.SEVERE, null, ex);
        }
       // System.out.println(s);
        return out;
    }
    
    public int addSupplier(){
        Connection con = db.dbconnect();
        String s = "insert into users(email,password,owner_name,company_name,regno,address,phone,user_type) values('"+email+"','"+password+"','"+owner_name+"','"+company_name+"','"+regno+"','"+address+"','"+phone+"','"+usertype+"')";
        try {
            PreparedStatement ps =con.prepareStatement(s, Statement.RETURN_GENERATED_KEYS);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            out = rs.getInt(1);
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Users.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out;
    }
    
    public int[] loginUser()
    {
        int data[]= new int[3];
        Connection con = db.dbconnect();
        String s = "select user_id,email,password,user_type,status from users where email ='"+email+"'";
        String newpass = null;
        try {
            PreparedStatement ps = con.prepareStatement(s);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                newpass = new String(Base64.encodeBase64(password.getBytes()));
                //System.out.println(newpass);
                if(newpass.equals(rs.getString("password"))){
                    data[0] = rs.getInt("user_id");
                    data[1] = rs.getInt("user_type");
                    data[2] = rs.getInt("status");
                    break;
                }
                else{
                    data[0] = 0;
                    data[1] = 0;
                    data[2] = 1;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Users.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }
    
    public List<Users> getAllUsers()
    {
        ResultSet rs;
        List<Users> list = new ArrayList();
        Connection con = db.dbconnect();
        String s ="select * from users left join orders on orders.user_id=users.user_id";
        //System.out.print(s);
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                Users u = new Users();
                u.setUserId(rs.getInt("user_id"));
                u.setEmail(rs.getString("email"));
                u.setOwnerName(rs.getString("owner_name"));
                u.setCompany(rs.getString("company_name"));
                u.setRegno(rs.getString("regno"));
                u.setAddress(rs.getString("address"));
                u.setPhone(rs.getString("phone"));
                u.setUsertype(rs.getInt("user_type"));
                u.setStatus(rs.getInt("status"));
                u.setPayment(rs.getInt("p_status"));
                list.add(u);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<Users> getUsers(int id)
    {
        ResultSet rs;
        List<Users> list = new ArrayList();
        Connection con = db.dbconnect();
        String s ="select * from users where user_id="+id;
        //System.out.print(s);
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                Users u = new Users();
                u.setUserId(rs.getInt("user_id"));
                u.setEmail(rs.getString("email"));
                u.setOwnerName(rs.getString("owner_name"));
                u.setCompany(rs.getString("company_name"));
                u.setRegno(rs.getString("regno"));
                u.setAddress(rs.getString("address"));
                u.setPhone(rs.getString("phone"));
                u.setUsertype(rs.getInt("user_type"));
                u.setStatus(rs.getInt("status"));
                list.add(u);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public int updateUser()
    {
        Connection con = db.dbconnect();
        String s = "update users set status = "+status+" where user_id="+user_id;
        try {
            PreparedStatement ps =con.prepareStatement(s);
            out = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Users.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out;
    }
}
