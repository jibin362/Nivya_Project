/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mysql.jdbc.Statement;
import connection.database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class plans {
    private int plan_id,order_id,user_id,status;
    private String plan_name,validity;
    private float amount;
    
    public int getPlanId()
    {
        return plan_id;
    }
    public int getOrderId()
    {
        return order_id;
    }
    public int getUserId()
    {
        return user_id;
    }
    public int getStatus()
    {
        return status;
    }
    public String getPlanName()
    {
        return plan_name;
    }
    public String getValidity()
    {
        return validity;
    }
    public float getAmount()
    {
        return amount;
    }
    
    public void setPlanId(int plan_id)
    {
        this.plan_id=plan_id;
    }
    public void setOrderId(int order_id)
    {
        this.order_id=order_id;
    }
    public void setUserId(int user_id)
    {
        this.user_id=user_id;
    }
    public void setStatus(int status)
    {
        this.status=status;
    }
    public void setPlanName(String plan_name)
    {
        this.plan_name=plan_name;
    }
    public void setValidity(String validity)
    {
        this.validity=validity;
    }
    public void setAmount(float amount)
    {
        this.amount=amount;
    }
    
    public int addPlan()
    {
        database db = new database();int i=0;
        Connection con = db.dbconnect();
        String s = "insert into plans(plan_name,amount,validity) values('"+plan_name+"',"+amount+",'"+validity+"')";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(plans.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public int addOrder()
    {
       database db = new database();int i=0;
        Connection con = db.dbconnect();
        String s = "insert into orders(plan_id,user_id,p_status) values("+plan_id+","+user_id+","+status+")";
        try {
            PreparedStatement ps = con.prepareStatement(s, Statement.RETURN_GENERATED_KEYS);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            i = rs.getInt(1);
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(plans.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i; 
    }
    
    public int updateOrder(int id)
    {
       database db = new database();int i=0;
        Connection con = db.dbconnect();
        String s = "update orders set p_status="+status+" where order_id="+id;
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(plans.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i; 
    }
    
    public List<plans> getPlans()
    {
        database db = new database();
        List<plans> list = new ArrayList();
        Connection con = db.dbconnect();
        String s = "select * from plans";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                plans p = new plans();
                p.setPlanId(rs.getInt("plan_id"));
                p.setPlanName(rs.getString("plan_name"));
                p.setAmount(rs.getFloat("amount"));
                p.setValidity(rs.getString("validity"));
                list.add(p);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(plans.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
