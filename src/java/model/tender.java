/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import connection.database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bozz
 */
public class tender {
    private String category,description,tender_name,t_description,last_date,post_date,live_date,company_name,owner_name;
    private int category_id,user_id,tender_id,tender_category,t_status;
    
    public String getCategory()
    {
        return category;
    }
    public String getDescription()
    {
        return description;
    }
    public int getCategoryId()
    {
        return category_id;
    }
    public int getUserId()
    {
        return user_id;
    }
    public int getTenderId()
    {
        return tender_id;
    }
    public int getTenderCategory()
    {
        return tender_category;
    }
    public String getTenderName()
    {
        return tender_name;
    }
    public String getTenderDescription()
    {
        return t_description;
    }
    public String getLastDate()
    {
        return last_date;
    }
    public String getLiveDate()
    {
        return live_date;
    }
    public String getPostDate()
    {
        return post_date;
    }
    public int getTstatus()
    {
        return t_status;
    }
    public String getCompanyName()
    {
        return company_name;
    }
    public String getOwnerName()
    {
        return owner_name;
    }
    
    public void setCategory(String category)
    {
        this.category=category;
    }
    public void setDescription(String description)
    {
        this.description=description;
    }
    public void setCategoryId(int category_id)
    {
        this.category_id=category_id;
    }
    public void setUserId(int user_id)
    {
        this.user_id=user_id;
    }
    public void setTenderId(int tender_id)
    {
        this.tender_id=tender_id;
    }
    public void setTenderCategory(int tender_category)
    {
        this.tender_category=tender_category;
    }
    public void setTendername(String tender_name)
    {
        this.tender_name=tender_name;
    }
    public void setTenderDescription(String t_description)
    {
        this.t_description=t_description;
    }
    public void setLastDate(String last_date)
    {
        this.last_date=last_date;
    }
    public void setPostDate(String post_date)
    {
        this.post_date=post_date;
    }
    public void setLiveDate(String live_date)
    {
        this.live_date=live_date;
    }
    public void setTstatus(int t_status)
    {
        this.t_status=t_status;
    }
    public void setCompanyName(String company_name)
    {
        this.company_name=company_name;
    }
    public void setOwnerName(String owner_name)
    {
        this.owner_name=owner_name;
    }
    
    public List<tender> getCategorys()
    {
        ResultSet rs;
        List<tender> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s = "select * from tender_categorys";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                tender t = new tender();
                t.setCategory(rs.getString("category_name"));
                t.setCategoryId(rs.getInt("category_id"));
                t.setDescription(rs.getString("description"));
                list.add(t);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public int addTender()
    {
        int i=0;
        database db = new database();
        Connection con = db.dbconnect();
        String s = "insert into tender(user_id,tender_category,tender_name,description,last_date,post_date,live_date,status) values("+user_id+","+tender_category+",'"+tender_name+"','"+t_description+"','"+last_date+"','"+post_date+"','"+live_date+"',1)";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public List<tender> getTenders(int user_id)
    {
        ResultSet rs;
        List<tender> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s ="select * from tender inner join tender_categorys on tender_categorys.category_id=tender.tender_category where user_id ='"+user_id+"'";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                tender t = new tender();
                t.setTenderId(rs.getInt("tender_id"));
                t.setTenderCategory(rs.getInt("tender_category"));
                t.setCategory(rs.getString("category_name"));
                t.setTendername(rs.getString("tender_name"));
                t.setTenderDescription(rs.getString("description"));
                t.setLastDate(rs.getString("last_date"));
                t.setLiveDate(rs.getString("live_date"));
                t.setPostDate(rs.getString("post_date"));
                t.setTstatus(rs.getInt("status"));
                list.add(t);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<tender> getTendersAll()
    {
        ResultSet rs;
        List<tender> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s ="select * from tender inner join tender_categorys on tender_categorys.category_id=tender.tender_category";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                tender t = new tender();
                t.setTenderId(rs.getInt("tender_id"));
                t.setTenderCategory(rs.getInt("tender_category"));
                t.setCategory(rs.getString("category_name"));
                t.setTendername(rs.getString("tender_name"));
                t.setTenderDescription(rs.getString("description"));
                t.setLastDate(rs.getString("last_date"));
                t.setLiveDate(rs.getString("live_date"));
                t.setPostDate(rs.getString("post_date"));
                t.setTstatus(rs.getInt("status"));
                list.add(t);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<tender> getTenderReport(String d1,String d2)
    {
        ResultSet rs;
        List<tender> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s ="select * from tender inner join tender_categorys on tender_categorys.category_id=tender.tender_category where post_date between '"+d1+"' and '"+d2+"'";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                tender t = new tender();
                t.setTenderId(rs.getInt("tender_id"));
                t.setTenderCategory(rs.getInt("tender_category"));
                t.setCategory(rs.getString("category_name"));
                t.setTendername(rs.getString("tender_name"));
                t.setTenderDescription(rs.getString("description"));
                t.setLastDate(rs.getString("last_date"));
                t.setLiveDate(rs.getString("live_date"));
                t.setPostDate(rs.getString("post_date"));
                t.setTstatus(rs.getInt("status"));
                list.add(t);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public int deleteTender(int id)
    {
        int i =0;
        database db = new database();
        Connection con = db.dbconnect();
        String s = "delete from tender where tender_id ="+id;
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public List<tender> editTender(int id)
    {
        ResultSet rs;
        List<tender> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s ="select * from tender where tender_id ='"+id+"'";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                tender t = new tender();
                t.setTenderId(rs.getInt("tender_id"));
                t.setTenderCategory(rs.getInt("tender_category"));
                t.setTendername(rs.getString("tender_name"));
                t.setTenderDescription(rs.getString("description"));
                t.setLastDate(rs.getString("last_date"));
                t.setLiveDate(rs.getString("live_date"));
                t.setTstatus(rs.getInt("status"));
                list.add(t);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<tender> viewTender(int id)
    {
        ResultSet rs;
        List<tender> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s ="select * from tender inner join tender_categorys on tender.tender_category=tender_categorys.category_id where tender_id ='"+id+"'";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                tender t = new tender();
                t.setTenderId(rs.getInt("tender_id"));
                t.setTenderCategory(rs.getInt("tender_category"));
                t.setCategory(rs.getString("category_name"));
                t.setTendername(rs.getString("tender_name"));
                t.setTenderDescription(rs.getString("description"));
                t.setLastDate(rs.getString("last_date"));
                t.setLiveDate(rs.getString("live_date"));
                t.setTstatus(rs.getInt("status"));
                list.add(t);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public int updateTender(int id)
    {
        int i =0;
        database db = new database();
        Connection con = db.dbconnect();
        String s = "update tender set tender_category = "+tender_category+",tender_name = '"+tender_name+"',description='"+t_description+"',last_date='"+last_date+"',post_date='"+post_date+"',live_date='"+post_date+"',status="+t_status+" where tender_id ="+id;
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public List<tender> getAllTenders()
    {
        ResultSet rs;
        List<tender> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s ="select * from tender inner join tender_categorys on tender_categorys.category_id=tender.tender_category inner join users on users.user_id=tender.user_id where tender.status = 1";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                tender t = new tender();
                t.setTenderId(rs.getInt("tender_id"));
                t.setCompanyName(rs.getString("company_name"));
                t.setTenderCategory(rs.getInt("tender_category"));
                t.setCategory(rs.getString("category_name"));
                t.setTendername(rs.getString("tender_name"));
                t.setTenderDescription(rs.getString("description"));
                t.setLastDate(rs.getString("last_date"));
                t.setLiveDate(rs.getString("live_date"));
                t.setPostDate(rs.getString("post_date"));
                t.setTstatus(rs.getInt("status"));
                list.add(t);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<tender> getBidTender(int id)
    {
        ResultSet rs;
        List<tender> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s ="select * from tender inner join tender_categorys on tender_categorys.category_id=tender.tender_category inner join users on users.user_id=tender.user_id where tender.status = 1 && tender_id ="+id;
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                tender t = new tender();
                t.setTenderId(rs.getInt("tender_id"));
                t.setCompanyName(rs.getString("company_name"));
                t.setOwnerName(rs.getString("owner_name"));
                t.setTenderCategory(rs.getInt("tender_category"));
                t.setCategory(rs.getString("category_name"));
                t.setTendername(rs.getString("tender_name"));
                t.setTenderDescription(rs.getString("description"));
                t.setLastDate(rs.getString("last_date"));
                t.setLiveDate(rs.getString("live_date"));
                t.setPostDate(rs.getString("post_date"));
                t.setTstatus(rs.getInt("status"));
                list.add(t);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public int suspendTender(int id)
    {
        int i =0;
        database db = new database();
        Connection con = db.dbconnect();
        String s = "update tender set status="+t_status+" where tender_id ="+id;
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
}
