<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<jsp:include page="includes/header.jsp" flush="true"></jsp:include>
<div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#" title="Post">Home</a></li>
                        <li class="active">Feedback</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div> 
<div class="content-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
            <form name="tender" action="addFeedback" method="post" class="">
            <% if(request.getParameter("stat")!=null){%>
                <p class="text-success text-center">Feedback Send Succesfully.</p>
                <% }else if(request.getParameter("alert")!=null){%>
                <p class="text-danger text-center">Failed to Send Feedback!</p>
                <% }%>
                <div class="col-md-12 form-group">
                    <label for="feedback">Feedback:&emsp;</label>
                    <textarea type="text" name="feedback" required="" rows="5" id="feedback" class="form-control" placeholder="Enter feedback"></textarea>
                </div>
                <div class="col-md-4">
                    <input type="hidden" name="type" id="type" value="bank">
                    <button type="submit" class="btn btn-info">Submit</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>
 <%
 }
 %>