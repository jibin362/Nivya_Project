<jsp:include page="includes/header.jsp" flush="true"></jsp:include>
 <div class="content-wrapper">
    <div class="container">
      <div class="row">
          <div class="col-lg-4 col-lg-offset-4 " style="margin-top: 50px">
            <form name="login" action="Login" method="post">
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input class="form-control" id="email" name="email" type="email" required="" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input class="form-control" id="password" name="password" type="password" required="" placeholder="Password">
                </div>
                    <button type="submit" class="btn btn-primary btn-block" >Login</button>
            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="register.jsp">Register an Account</a>
            </div>
            <div style="padding: 20px">
                <%
                    if(request.getParameter("alert")!=null)
                    {%>
                    <p class="text-center text-warning">Profile Verification Failed!</p>   
                   <% }
                      else if(request.getParameter("error")!=null)
                      {%>
                      <p class="text-center text-danger">Invalid Login Credentials!</p>
                    <%}
                %>
            </div>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>