/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import connection.database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class feeds {
    private int feed_id,user_id;
    private String feedback,feed_date,company;
    
    public int getFeedId()
    {
        return feed_id;
    }
    public int getUserId()
    {
        return user_id;
    }
    public String getFeedback()
    {
        return feedback;
    }
    public String getFeedDate()
    {
        return feed_date;
    }
    public String getCompany()
    {
        return company;
    }
    
    public void setFeedId(int feed_id)
    {
        this.feed_id=feed_id;
    }
    public void setUserId(int user_id)
    {
        this.user_id=user_id;
    }
    public void setFeedback(String feedback)
    {
        this.feedback=feedback;
    }
    public void setFeedDate(String feed_date)
    {
        this.feed_date=feed_date;
    }
    public void setCompany(String company)
    {
        this.company=company;
    }
    
    public int addFeed()
    {
        int i =0;
        database db = new database();
        Connection con = db.dbconnect();
        String s = "insert into feedback(user_id,feedback,feed_date) values("+user_id+",'"+feedback+"','"+feed_date+"')";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(feeds.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public List<feeds> listFeeds()
    {
        List<feeds> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s = "select * from feedback inner join users on feedback.user_id=users.user_id";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                feeds f = new feeds();
                f.setFeedId(rs.getInt("feed_id"));
                f.setFeedback(rs.getString("feedback"));
                f.setUserId(rs.getInt("user_id"));
                f.setCompany(rs.getString("company_name"));
                f.setFeedDate(rs.getString("feed_date"));
                list.add(f);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(feeds.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<feeds> getFeed(int id)
    {
        List<feeds> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s = "select * from feeds where feed_id="+id;
        try {
            PreparedStatement ps = con.prepareStatement(s);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                feeds f = new feeds();
                f.setFeedId(rs.getInt("feed_id"));
                f.setFeedback(rs.getString("feedback"));
                f.setUserId(rs.getInt("user_id"));
                f.setFeedDate(rs.getString("feed_date"));
                list.add(f);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(feeds.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
