<%@page import="model.live"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="includes/header_supplier.jsp" flush="true"></jsp:include>
<div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#" title="Post">Home</a></li>
                        <li class="active">Live Bids</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>  
<div class="content-wrapper">
    <div class="container">
      <div class="row card-body">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <form name="live_tender" action="liveBid" method="post">
                <div class="col-sm-4 form-group">
                    <input type="text" name="amount" id="amount" required="" class="form-control" placeholder="Enter Bid Amount">
                </div>
                <div class="col-sm-4 form-group">
                    <input type="text" name="max_day" id="max_day" required="" class="form-control" placeholder="Enter Max Days">
                </div>
                <button class="btn btn-success" id="live-btn">Add</button>
                </form>
            </div>
            <br>
            <table class="table table-bordered">
                <thead>
                    <th>Sl No</th>
                    <th>Bid Amount</th>
                    <th>Max Days</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    <%
                    live l = new live();
                    List<live> list = l.getBid();
                    Iterator itr = list.iterator();
                    int i=0;
                    while(itr.hasNext())
                    {
                        l = (live)itr.next();
                        i++;
                    %>
                    <tr><td><%=i%></td>
                        <td><%=l.getLiveAmount() %></td>
                        <td><%=l.getLiveMaxdays() %></td>
                        <td><% if(l.getLbStatus()==0)
                            {%>
                            <span class="btn-sm btn-success">Active</span>
                        <%  }else{
                            %>
                                <span class="btn-sm btn-warning">Inactive</span>
                            <%
                            }
                            %>
                        </td>
                    </tr>
                    <%
                    }
                    %>
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>
 <%}%>