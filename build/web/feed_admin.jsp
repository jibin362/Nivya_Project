<%@page import="model.feeds"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<jsp:include page="includes/header_admin.jsp" flush="true"></jsp:include>
 <div class="content-wrapper">
    <div class="container-fluid">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">View Feedback</li>
      </ol>
      <div class="row card-body">
        <div class="col-sm-12">
            <table class="table table-bordered">
                <thead>
                    <th>Sl No</th>
                    <th>User</th>
                    <th>Feedback</th>
                    <th>Date</th>
                </thead>
                <tbody>
                <% 
                    feeds f = new feeds();int i=1;
                    List<feeds> list = f.listFeeds();
                    Iterator itr = list.iterator();
                    while(itr.hasNext())
                    {
                        f = (feeds)itr.next();
                %>
                <tr><td><%=i%></td>
                        <td><%=f.getCompany() %></td>
                        <td><%=f.getFeedback() %></td>
                        <td><%=f.getFeedDate() %></td>
                </tr>
                <% i++;} %>
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer_1.jsp" flush="true"></jsp:include>
 <%
 }
 %>