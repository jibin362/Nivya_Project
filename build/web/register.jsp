<jsp:include page="includes/header.jsp" flush="true"></jsp:include>
 <div class="content-wrapper">
    <div class="container">
        <div class="col-lg-6 col-lg-offset-3">
            <h1 class="text-center" id="reg-header">Register an Account as Banker</h1>
            <div class="card-body" id="company-reg">
              <form action="Register" method="post">
              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-6">
                    <label for="exampleInputName">Email</label>
                    <input class="form-control" id="email" name="email" type="email" aria-describedby="nameHelp" placeholder="Enter email" required="">
                  </div>
                  <div class="col-md-6">
                    <label for="exampleInputLastName">Authority Name</label>
                    <input class="form-control" id="owner_name" name="owner_name" type="text" aria-describedby="nameHelp" placeholder="Enter owner name" required="">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-6">
                    <label for="exampleInputName">Bank Name</label>
                    <input class="form-control" id="company_name" name="company_name" type="text" aria-describedby="nameHelp" placeholder="Enter company name" required="">
                  </div>
                  <div class="col-md-6">
                    <label for="exampleInputLastName">Register No</label>
                    <input class="form-control" id="regno" name="regno" type="text" aria-describedby="nameHelp" placeholder="Enter company register no" required="">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-6">
                    <label for="exampleInputName">Address</label>
                    <input class="form-control" id="address" name="address" type="text" aria-describedby="nameHelp" placeholder="Enter company address" required="">
                  </div>
                  <div class="col-md-6">
                    <label for="exampleInputLastName">Phone</label>
                    <input class="form-control" id="phone" name="phone" type="text" aria-describedby="nameHelp" placeholder="Enter phone number" required="">
                  </div>
                </div>
              </div>
              <div class="form-group" >
                <div class="form-row">
                  <div class="col-md-6">
                    <label for="exampleInputPassword1">Password</label>
                    <input class="form-control" id="password" name="password" type="password" placeholder="Password" required="">
                  </div>
                  <div class="col-md-6">
                    <label for="exampleConfirmPassword">Confirm password</label>
                    <input class="form-control" id="cpassword" name="cpassword" type="password" placeholder="Confirm password" required="">
                  </div>
                </div>
              </div>
                  <button class="btn btn-primary btn-block" onclick="return(checkpass())">Register</button>
            </form>
          </div>

          <div class="card-body hide" id="supplier-reg">
              <form action="Supplier" method="post">
              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-6">
                    <label for="exampleInputName">Email</label>
                    <input class="form-control" id="email" name="semail" type="email" aria-describedby="nameHelp" placeholder="Enter email" required="">
                  </div>
                  <div class="col-md-6">
                    <label for="exampleInputLastName">Authority Name</label>
                    <input class="form-control" id="owner_name" name="sowner_name" type="text" aria-describedby="nameHelp" placeholder="Enter owner name" required="">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-6">
                    <label for="exampleInputName">Company Name</label>
                    <input class="form-control" id="company_name" name="scompany_name" type="text" aria-describedby="nameHelp" placeholder="Enter company name" required="">
                  </div>
                  <div class="col-md-6">
                    <label for="exampleInputLastName">Register No</label>
                    <input class="form-control" id="regno" name="sregno" type="text" aria-describedby="nameHelp" placeholder="Enter company register no" required="">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-6">
                    <label for="exampleInputName">Address</label>
                    <input class="form-control" id="address" name="saddress" type="text" aria-describedby="nameHelp" placeholder="Enter company address" required="">
                  </div>
                  <div class="col-md-6">
                    <label for="exampleInputLastName">Phone</label>
                    <input class="form-control" id="sphone" name="sphone" type="text" aria-describedby="nameHelp" placeholder="Enter phone number" required="">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-6">
                    <label for="exampleInputPassword1">Password</label>
                    <input class="form-control" id="spassword" name="spassword" type="password" placeholder="Password" required="">
                  </div>
                  <div class="col-md-6">
                    <label for="exampleConfirmPassword">Confirm password</label>
                    <input class="form-control" id="scpassword" name="scpassword" type="password" placeholder="Confirm password" required="">
                  </div>
                </div>
              </div>
                  <div style="form-group">
                  <button class="btn btn-primary btn-block" onclick="return(checksupp())">Register</button>
                  </div>
              </form>
          </div>
        </div>
        <div class="col-lg-4 col-lg-offset-4" style="padding: 20px">
              <div class="text-center">
                  <button id="reg-company" class="btn btn-info hide">Register as Bank</button>
                  <button id="reg-supplier" class="btn btn-info">Register as Supplier</button>
              </div>
          </div>
      </div>
    </div>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>
