<%@page import="java.util.Base64"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="includes/header_supplier.jsp" flush="true"></jsp:include>
<div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#" title="Post">Home</a></li>
                        <li class="active">View Tender</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div> 
<div class="content-wrapper">
    <div class="container">
      <div class="row card-body">
        <div class="col-sm-12">
            <table class="table table-bordered">
                <thead>
                    <th>Sl No</th>
                    <th>Tender</th>
                    <th>Category</th>
                    <th>Post date</th>
                    <th>Live Date</th>
                    <th>Last date</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    <%
                    tender t = new tender();
                    List<tender> list = t.getAllTenders();
                    Iterator itr = list.iterator();
                    int i=0;
                    while(itr.hasNext())
                    {
                        t = (tender)itr.next();
                        i++;
                    %>
                    <tr><td><%=i%></td>
                        <td><%=t.getTenderName()%></td>
                        <td><%=t.getCategory() %></td>
                        <td><%=t.getPostDate() %></td>
                        <td><%=t.getLiveDate() %></td>
                        <td><%=t.getLastDate() %></td>
                        <% 
                        String id = String.valueOf(t.getTenderId());
                        String link = new String(Base64.getUrlEncoder().encodeToString(id.getBytes())); %>
                        <td><a href="view.jsp?tid=<%=t.getTenderId() %>" class="btn-sm btn-info">View</a>&nbsp;
                            <a href="bidon.jsp?id=<%=t.getTenderId()%>" class="btn-sm btn-success bid">Bid</a></td>
                    </tr>
                    <%
                    }
                    %>
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>
 <%}%>