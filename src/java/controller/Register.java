/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Users;
import org.apache.tomcat.util.codec.binary.Base64;

/**
 *
 * @author Bozz
 */
public class Register extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Users u = new Users();
        u.setEmail(request.getParameter("email"));
        String password = new String(Base64.encodeBase64(request.getParameter("password").getBytes()));
        u.setPassword(password);
        u.setOwnerName(request.getParameter("owner_name"));
        String name = request.getParameter("company_name");
        u.setCompany(request.getParameter("company_name"));
        u.setRegno(request.getParameter("regno"));
        u.setAddress(request.getParameter("address"));
        u.setPhone(request.getParameter("phone"));
        u.setUsertype(1);
        int i = u.addUser();
        if(i>0){
            HttpSession session = request.getSession();
            session.setAttribute("regname",name);
            RequestDispatcher rd = request.getRequestDispatcher("plans.jsp");
            request.setAttribute("user_id", i);
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("register.jsp?error=1");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
