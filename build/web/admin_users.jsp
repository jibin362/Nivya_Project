<%@page import="model.Users"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="includes/header_admin.jsp" flush="true"></jsp:include>
 <div class="content-wrapper">
    <div class="container-fluid">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">All Users</li>
      </ol>
      <div class="row card-body">
        <div class="col-sm-12">
            <table class="table table-bordered">
                <thead>
                    <th>Sl No</th>
                    <th>Company Name</th>
                    <th>Reg No</th>
                    <th>Phone</th>
                    <th>Payment</th>
                    <th>User Type</th>
                    <th>Status</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    <%
                    Users u = new Users();
                    List<Users> list = u.getAllUsers();
                    Iterator itr = list.iterator();
                    int i=0;
                    while(itr.hasNext())
                    {
                        u = (Users)itr.next();
                        i++;
                    %>
                    <tr><td><%=i%></td>
                        <td><%=u.getCompany() %></td>
                        <td><%=u.getRegno() %></td>
                        <td><%=u.getPhone() %></td>
                        <td><%=u.getPayment() %></td>
                        <td><%if(u.getUsertype()==1){%>
                            <span class="btn-sm btn-info">Bank</span>
                            <% }else{%>
                            <span class="btn-sm btn-info">Supplier</span>
                            <% }%>
                        </td>
                        <td><% if(u.getStatus()==1)
                            {%>
                                <span class="btn-sm btn-success">Active</span>
                            <%}else if(u.getStatus()==2){
                            %>
                                <span class="btn-sm btn-danger">Inactive</span>
                            <%
                              }else{
                            %>
                                <span class="btn-sm btn-warning">Suspended</span>
                            <%
                            }
                            %>
                        </td>
                        <td><a href="view_user.jsp?id=<%=u.getUserId() %>" class="btn-sm btn-info">View</a>&nbsp;
                            <a href="#" class="btn-sm btn-success uaccept" data-id="<%=u.getUserId() %>">Accept</a>&nbsp;
                            <a href="#" class="btn-sm btn-danger ureject" data-id="<%=u.getUserId() %>" >Reject</a></td>
                    </tr>
                    <%
                    }
                    %>
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer_1.jsp" flush="true"></jsp:include>
 <%}%>