package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "includes/header.jsp", out, true);
      out.write("\n");
      out.write(" <div class=\"content-wrapper\">\n");
      out.write("    <div class=\"container\">\n");
      out.write("      <div class=\"row\">\n");
      out.write("        <div class=\"col-12\">\n");
      out.write("            <form name=\"login\" action=\"Login\" method=\"post\">\n");
      out.write("                <div class=\"form-group\">\n");
      out.write("                  <label for=\"exampleInputEmail1\">Email address</label>\n");
      out.write("                  <input class=\"form-control\" id=\"email\" name=\"email\" type=\"email\" required=\"\" aria-describedby=\"emailHelp\" placeholder=\"Enter email\">\n");
      out.write("                </div>\n");
      out.write("                <div class=\"form-group\">\n");
      out.write("                  <label for=\"exampleInputPassword1\">Password</label>\n");
      out.write("                  <input class=\"form-control\" id=\"password\" name=\"password\" type=\"password\" required=\"\" placeholder=\"Password\">\n");
      out.write("                </div>\n");
      out.write("                    <button type=\"submit\" class=\"btn btn-primary btn-block\" >Login</button>\n");
      out.write("            </form>\n");
      out.write("            <div class=\"text-center\">\n");
      out.write("                <a class=\"d-block small mt-3\" href=\"register.jsp\">Register an Account</a>\n");
      out.write("            </div>\n");
      out.write("            <div style=\"padding: 20px\">\n");
      out.write("                ");

                    if(request.getParameter("alert")!=null)
                    {
      out.write("\n");
      out.write("                    <p class=\"text-center text-warning\">Profile Verification Failed!</p>   \n");
      out.write("                   ");
 }
                      else if(request.getParameter("error")!=null)
                      {
      out.write("\n");
      out.write("                      <p class=\"text-center text-danger\">Invalid Login Credentials!</p>\n");
      out.write("                    ");
}
                
      out.write("\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write(" ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "includes/footer.jsp", out, true);
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
