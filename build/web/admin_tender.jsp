<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="includes/header_admin.jsp" flush="true"></jsp:include>
 <div class="content-wrapper">
    <div class="container-fluid">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Add Tender</li>
      </ol>
      <div class="row card-body">
        <div class="col-sm-12">
            <table class="table table-bordered">
                <thead>
                    <th>Sl No</th>
                    <th>Tender</th>
                    <th>Category</th>
                    <th>Post date</th>
                    <th>Live Date</th>
                    <th>Last date</th>
                    <th>Status</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    <%
                    tender t = new tender();
                    List<tender> list = t.getTendersAll();
                    Iterator itr = list.iterator();
                    int i=0;
                    while(itr.hasNext())
                    {
                        t = (tender)itr.next();
                        i++;
                    %>
                    <tr><td><%=i%></td>
                        <td><%=t.getTenderName()%></td>
                        <td><%=t.getCategory() %></td>
                        <td><%=t.getPostDate() %></td>
                        <td><%=t.getLiveDate() %></td>
                        <td><%=t.getLastDate() %></td>
                        <td><% if(t.getTstatus()==1)
                            {%>
                                <span class="btn-sm btn-success">Active</span>
                            <%}else if(t.getTstatus()==0){
                            %>
                                <span class="btn-sm btn-danger">Inactive</span>
                            <%
                              }else if(t.getTstatus()==3){
                            %>
                                <span class="btn-sm btn-info">Clossed</span>
                            <% }else{
                            %>
                                <span class="btn-sm btn-warning">Suspended</span>
                            <%
                            }
                            %>
                        </td>
                        <td><a href="aview_tender.jsp?tid=<%=t.getTenderId()%>" class="btn-sm btn-info">View</a>&nbsp;
                            <a href="#" class="btn-sm btn-danger delete" data-id="<%=t.getTenderId()%>" >Delete</a></td>
                    </tr>
                    <%
                    }
                    %>
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer_1.jsp" flush="true"></jsp:include>
 <%}%>