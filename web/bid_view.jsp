<%@page import="model.live"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
        response.setIntHeader("Refresh", 20);
%>
<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="includes/header.jsp" flush="true"></jsp:include>
<div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#" title="Post">Home</a></li>
                        <li class="active">Live Bids</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>  
<div class="content-wrapper">
    <div class="container">
      <div class="row card-body">
        <div class="col-sm-12">
            <table class="table table-bordered">
                <thead>
                    <th>Sl No</th>
                    <th>Bid Amount</th>
                    <th>Max Days</th>
                    <th>Status</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    <%
                    live l = new live();
                    int tid = Integer.parseInt(request.getParameter("id"));
                    List<live> list = l.getBid();
                    Iterator itr = list.iterator();
                    int i=0;
                    while(itr.hasNext())
                    {
                        l = (live)itr.next();
                        i++;
                    %>
                    <tr><td><%=i%></td>
                        <td><%=l.getLiveAmount() %></td>
                        <td><%=l.getLiveMaxdays() %></td>
                        <td><% if(l.getLbStatus()==0)
                            {%>
                            <span class="btn-sm btn-warning">Pending</span>
                        <%  }else if(l.getLbStatus()==1)
                            {%>
                            <span class="btn-sm btn-success">Accepted</span>
                        <%  }else{
                            %>
                                <span class="btn-sm btn-warning">Inactive</span>
                            <%
                            }
                            %>
                        </td>
                        <td>
                            <a href="#" class="btn-sm btn-success liveaccept" data-id="<%=l.getLbId() %>" data-tid="<%=tid%>">Accept</a>
                            <a href="#" class="btn-sm btn-danger livereject" data-id="<%=l.getLbId() %>">Reject</a>
                        </td>
                    </tr>
                    <%
                    }
                    %>
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>
 <%}%>