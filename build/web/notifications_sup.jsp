<%@page import="model.notice"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="includes/header_supplier.jsp" flush="true"></jsp:include>
    <div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#" title="Post">Home</a></li>
                        <li class="active">Notifications</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>
    <main class="site-main category-main">
        <div class="container">
            <div class="row">
                <section class="category-content col-sm-9">
                    <ul class="media-list">
                        <%
                notice n = new notice();
                List<notice> list = n.allNotice();
                Iterator itr = list.iterator();
                int i=0;
                while(itr.hasNext())
                {
                    n = (notice)itr.next();
                    i++;
                %>
                        <li class="media">
                            <div class="media-left">
                                <a href="#" title="Post">
                                    <img class="media-object" src="img/h1.png" height="200px" alt="Post">
                                </a>
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading"><%=n.getName() %></h3>
                                <p><%=n.getDetails() %></p>
                                <aside class="meta category-meta">
                                    <div class="pull-left">
                                        <div class="arc-date"><p>Posted by Admin on <%=n.getDate() %></p></div>
                                    </div>
                                </aside>                                
                            </div>
                        </li>
                        <%
                }
                %>
                    </ul>                    
                </section>
            </div>
        </div>
    </main>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>
 <%}%>