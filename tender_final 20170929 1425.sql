-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.2.3-falcon-alpha-community-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema tender
--

CREATE DATABASE IF NOT EXISTS tender;
USE tender;

--
-- Definition of table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`admin_id`,`email`,`password`) VALUES 
 (1,'admin@gmail.com','MTIz');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;


--
-- Definition of table `bids`
--

DROP TABLE IF EXISTS `bids`;
CREATE TABLE `bids` (
  `bid_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tender_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `amount` varchar(45) NOT NULL,
  `bid_date` varchar(45) NOT NULL,
  `bid_status` int(10) unsigned NOT NULL,
  `details` varchar(2000) NOT NULL,
  `work_days` int(10) unsigned NOT NULL,
  PRIMARY KEY (`bid_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bids`
--

/*!40000 ALTER TABLE `bids` DISABLE KEYS */;
INSERT INTO `bids` (`bid_id`,`tender_id`,`user_id`,`amount`,`bid_date`,`bid_status`,`details`,`work_days`) VALUES 
 (2,7,20,'5000','09/26/2017',1,'hsbcbacjanskmsaklmdl',10),
 (3,9,20,'2000','09/30/2017',3,'sdas',30);
/*!40000 ALTER TABLE `bids` ENABLE KEYS */;


--
-- Definition of table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `feed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `feedback` varchar(500) NOT NULL,
  `feed_date` varchar(45) NOT NULL,
  PRIMARY KEY (`feed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` (`feed_id`,`user_id`,`feedback`,`feed_date`) VALUES 
 (1,15,'uguib najm sasddaskd pasmxas klnasb xjknam ,xlas mxasbx nkal','10/01/2017'),
 (2,20,'asihsi uah dihas asxos aijos aoxj oisjixo sajnxk ahxik ans iasih xian','10/01/2017'),
 (3,20,'hbc hjsjn csh cajo kdla sda bsjd baj cns acj','10/01/2017');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;


--
-- Definition of table `live_bid`
--

DROP TABLE IF EXISTS `live_bid`;
CREATE TABLE `live_bid` (
  `lb_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `live_max_days` int(10) unsigned NOT NULL,
  `live_amount` float NOT NULL,
  `lb_status` int(10) unsigned NOT NULL,
  PRIMARY KEY (`lb_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `live_bid`
--

/*!40000 ALTER TABLE `live_bid` DISABLE KEYS */;
INSERT INTO `live_bid` (`lb_id`,`user_id`,`live_max_days`,`live_amount`,`lb_status`) VALUES 
 (2,20,10,1000,1);
/*!40000 ALTER TABLE `live_bid` ENABLE KEYS */;


--
-- Definition of table `live_tender`
--

DROP TABLE IF EXISTS `live_tender`;
CREATE TABLE `live_tender` (
  `live_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `live_name` varchar(45) NOT NULL,
  `live_date` varchar(45) NOT NULL,
  `live_details` varchar(2000) NOT NULL,
  `lt_status` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`live_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `live_tender`
--

/*!40000 ALTER TABLE `live_tender` DISABLE KEYS */;
INSERT INTO `live_tender` (`live_id`,`live_name`,`live_date`,`live_details`,`lt_status`,`user_id`) VALUES 
 (2,'Live Tender One','10/01/2017','sf sdv dfsf vsfd bfdv',2,15);
/*!40000 ALTER TABLE `live_tender` ENABLE KEYS */;


--
-- Definition of table `notice`
--

DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `n_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `details` varchar(5000) NOT NULL,
  `date` varchar(45) NOT NULL,
  PRIMARY KEY (`n_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice`
--

/*!40000 ALTER TABLE `notice` DISABLE KEYS */;
INSERT INTO `notice` (`n_id`,`name`,`details`,`date`) VALUES 
 (3,'Notice One','Notice from admin','09/27/2017'),
 (4,'Notice two','Some random information to banks','09/28/2017'),
 (5,'Notice three','some random information to suppliers','09/29/2017');
/*!40000 ALTER TABLE `notice` ENABLE KEYS */;


--
-- Definition of table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plan_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `p_status` int(10) unsigned NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`order_id`,`plan_id`,`user_id`,`p_status`) VALUES 
 (1,1,28,1);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;


--
-- Definition of table `plans`
--

DROP TABLE IF EXISTS `plans`;
CREATE TABLE `plans` (
  `plan_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(45) NOT NULL,
  `amount` float NOT NULL,
  `validity` varchar(45) NOT NULL,
  PRIMARY KEY (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plans`
--

/*!40000 ALTER TABLE `plans` DISABLE KEYS */;
INSERT INTO `plans` (`plan_id`,`plan_name`,`amount`,`validity`) VALUES 
 (1,'Plan 1',299,'Three Months'),
 (2,'Plan 2',499,'Six Months'),
 (3,'Plan 3',799,'One Year');
/*!40000 ALTER TABLE `plans` ENABLE KEYS */;


--
-- Definition of table `tender`
--

DROP TABLE IF EXISTS `tender`;
CREATE TABLE `tender` (
  `tender_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `tender_category` int(10) unsigned NOT NULL,
  `tender_name` varchar(45) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `last_date` varchar(45) NOT NULL,
  `post_date` varchar(45) NOT NULL,
  `live_date` varchar(45) NOT NULL,
  `status` int(10) unsigned NOT NULL,
  PRIMARY KEY (`tender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tender`
--

/*!40000 ALTER TABLE `tender` DISABLE KEYS */;
INSERT INTO `tender` (`tender_id`,`user_id`,`tender_category`,`tender_name`,`description`,`last_date`,`post_date`,`live_date`,`status`) VALUES 
 (7,15,1,'Tender One','Tender for construction bank one','09/30/2017','10/01/2017','10/01/2017',3),
 (8,15,3,'Tender two','Tender for computers bank one','09/30/2017','09/28/2017','09/28/2017',1),
 (9,16,3,'Tender One','Computers Purchase Tender','09/30/2017','09/22/2017','09/22/2017',1);
/*!40000 ALTER TABLE `tender` ENABLE KEYS */;


--
-- Definition of table `tender_categorys`
--

DROP TABLE IF EXISTS `tender_categorys`;
CREATE TABLE `tender_categorys` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tender_categorys`
--

/*!40000 ALTER TABLE `tender_categorys` DISABLE KEYS */;
INSERT INTO `tender_categorys` (`category_id`,`category_name`,`description`) VALUES 
 (1,'Construction','Tenders related to construction works'),
 (2,'Electrical','Tenders related to electrical works'),
 (3,'Tenchnology','Tenders related to computers and technology'),
 (4,'Stationary','Tenders related to stationary items');
/*!40000 ALTER TABLE `tender_categorys` ENABLE KEYS */;


--
-- Definition of table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(64) NOT NULL,
  `owner_name` varchar(45) NOT NULL,
  `company_name` varchar(45) NOT NULL,
  `regno` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `user_type` int(10) unsigned NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`,`email`,`password`,`owner_name`,`company_name`,`regno`,`address`,`phone`,`user_type`,`status`) VALUES 
 (15,'bank1@gmail.com','MTIz','Manager','Bank One','B001','asdas','9497526341',1,1),
 (16,'bank2@gmail.com','MTIz','Manager','Bank two','B002','fghdf','8574965241',1,1),
 (20,'supplier1@gmail.com','MTIz','One','Supplier One','S001','fghdfgsd','8574965263',2,1),
 (21,'bank3@gmail.com','MTIzQGFiY2Q=','Manager','Bank3','123','sdfsf','12313213',1,2),
 (28,'bank4@gmail.com','MTIz','Manager','Bank 4','123','sdfsf','8785678567',1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
