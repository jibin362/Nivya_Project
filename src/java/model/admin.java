/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import connection.database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.tomcat.util.codec.binary.Base64;

/**
 *
 * @author DELL
 */
public class admin {
    private int admin_id;
    private String email,password;
    
    public int getAdminId()
    {
        return admin_id;
    }
    public String getEmail()
    {
        return email;
    }
    public String getPassword()
    {
        return password;
    }
    public void setAdminId(int admin_id)
    {
        this.admin_id=admin_id;
    }
    public void setEmail(String email)
    {
        this.email=email;
    }
    public void setPassword(String password)
    {
        this.password=password;
    }
    
    public int loginAdmin()
    {
        int i=0;
        database db = new database();
        Connection con = db.dbconnect();
        String newpass = password;
        String s = "select * from admin where email ='"+email+"'";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                newpass = new String(Base64.encodeBase64(password.getBytes()));
                //System.out.println(newpass);
                if(newpass.equals(rs.getString("password"))){
                    i = rs.getInt("admin_id");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
}
