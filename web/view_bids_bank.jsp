<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<%@page import="model.bid"%>
<%@page import="java.util.Base64"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="includes/header.jsp" flush="true"></jsp:include>
<div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#" title="Post">Home</a></li>
                        <li class="active">View Bids</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>
<div class="content-wrapper">
    <div class="container">
      <div class="row card-body">
        <div class="col-sm-12">
            <table class="table table-bordered">
                <thead>
                    <th>Sl No</th>
                    <th>Tender Name</th>
                    <th>Amount</th>
                    <th>Bid Date</th>
                    <th>Work days</th>
                    <th>Details</th>
                    <th>Status</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    <%
                        int user_id = (int)(session.getAttribute("user_id"));
                    bid b = new bid();
                    List<bid> list = b.getBidsBank(user_id);
                    Iterator itr = list.iterator();
                    int i=0;
                    while(itr.hasNext())
                    {
                        b = (bid)itr.next();
                        i++;
                    %>
                    <tr><td><%=i%></td>
                        <td><%=b.getTenderName() %></td>
                        <td><%=b.getAmount() %></td>
                        <td><%=b.getBidDate() %></td>
                        <td><%=b.getWorkDays() %></td>
                        <td><%=b.getBidDetails() %></td>
                        <td><%if(b.getBidStatus()==0)
                            {%>
                                <span class="btn-sm btn-warning">Pending</span>
                            <%}else if(b.getBidStatus()==1){
                            %>
                                <span class="btn-sm btn-success">Accepted</span>
                            <%
                              }else if(b.getBidStatus()==2){
                            %>
                                <span class="btn-sm btn-success">Rejected</span>
                            <%
                              }else{
                            %>
                                <span class="btn-sm btn-warning">Re-Submitted</span>
                            <%
                            }
                            %>
                        </td>
                        <% 
                        String bid = String.valueOf(b.getBidId());
                        String link = new String(Base64.getUrlEncoder().encodeToString(bid.getBytes())); %>
                        <td><% if(b.getBidStatus()==1){}else{ %>
                            <a href="#" data-id="<%=b.getBidId() %>" data-tid="<%=b.getTenderId() %>" class="btn-sm btn-success accept">Accept</a>&nbsp;
                            <a href="#" data-id="<%=b.getBidId() %>" class="btn-sm btn-danger reject">Reject</a>
                            <%} %>
                        </td>
                    </tr>
                    <%
                    }
                    %>
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>
 <%}%>