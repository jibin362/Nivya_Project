<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="model.plans"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
    <h2><p class="text-center text-info">Order a Plan</p></h2>
  <div class="row">
      <%
          plans p = new plans();
          int id = (Integer) request.getAttribute("user_id");
          List<plans> list = p.getPlans();
          Iterator itr = list.iterator();
          while(itr.hasNext())
          {
              p = (plans)itr.next();
        %>
        <div class="col-md-4 card card-login mx-auto mt-5">
            <div class="text-center">
                <h3><%=p.getPlanName() %></h3>
                <hr>
                <p><span class="fa fa-inr"></span> <%=p.getAmount() %></p>
                <p><%=p.getValidity() %></p>
                <p><button class="btn-sm btn-info plan" data-id="<%=p.getPlanId() %>" data-cost="<%=p.getAmount() %>" data-uid="<%=id %>" >Order Now</button></p>
            </div>
        </div>
        <%
          }
      %>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/functions.js"></script>
</body>

</html>
