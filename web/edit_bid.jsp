<%@page import="model.bid"%>
<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
    String tid = request.getParameter("id");
%>
<jsp:include page="includes/header_supplier.jsp" flush="true"></jsp:include>
<div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#" title="Post">Home</a></li>
                        <li class="active">Edit Bid</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>  
<div class="content-wrapper">
    <div class="container">
      <div class="row card-body">
          <div class="col-md-12">
              <form class="" name="bid_form" action="Edit_bid" method="post">
                  <%
                    bid b = new bid();
                    b = b.getBid(Integer.parseInt(request.getParameter("bid")));
                    %>
                <div class="col-md-4 form-group">
                    <label for="amount">Bid Amount</label>
                    <input type="number" name="amount" id="amount" class="form-control" value="<%=b.getAmount() %>" placeholder="Enter bid amount">
                    <input type="hidden" name="tender_id" id="tender_id" value="<%=b.getTenderId() %>">
                    <input type="hidden" name="bid_id" id="bid_id" value="<%=request.getParameter("bid")%>">
                </div>
                <div class="col-md-4 form-group">
                    <label for="amount">Estimated Work Days</label>
                    <input type="number" name="work_days" id="work_days" class="form-control" placeholder="Enter Estimated Days" value="<%=b.getWorkDays() %>">
                </div>
                  <div class="col-md-12 form-group">
                      <label for="details">Details<span>(if any)</span></label>
                      <textarea name="details" id="details" class="form-control" rows="5" placeholder="Enter details (if any)"><%=b.getBidDetails() %></textarea>
                  </div>
                  <div class="col-md-4 form-group">
                      <input type="submit" class="btn btn-success" value="Submit">
                  </div>
              </form>
          </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>
 <%
 }
 %>