<%@page import="model.notice"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="includes/header_admin.jsp" flush="true"></jsp:include>
 <div class="content-wrapper">
    <div class="container-fluid">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">View Notice</li>
      </ol>
      <div class="row card-body">
        <div class="col-sm-12">
            <table class="table table-bordered">
                <thead>
                    <th>Sl No</th>
                    <th>Notice</th>
                    <th>Details</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    <%
                    notice n = new notice();
                    List<notice> list = n.allNotice();
                    Iterator itr = list.iterator();
                    int i=0;
                    while(itr.hasNext())
                    {
                        n = (notice)itr.next();
                        i++;
                    %>
                    <tr><td><%=i%></td>
                        <td><%=n.getName() %></td>
                        <td><%=n.getDetails() %></td>
                        <td><a href="edit_notice.jsp?tid=<%=n.getNid() %>" class="btn-sm btn-info">Edit</a>&nbsp;
                            <a href="#" class="btn-sm btn-danger ndelete" data-id="<%=n.getNid() %>" >Delete</a></td>
                    </tr>
                    <%
                    }
                    %>
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer_1.jsp" flush="true"></jsp:include>
 <%}%>