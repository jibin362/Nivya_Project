<%@page import="model.live"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="includes/header.jsp" flush="true"></jsp:include>
<div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#" title="Post">Home</a></li>
                        <li class="active">Live Bids</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

<div class="content-wrapper">
    <div class="container">
      <div class="row card-body">
        <div class="col-sm-12">
            <table class="table table-bordered">
                <thead>
                    <th>Sl No</th>
                    <th>Tender Name</th>
                    <th>Details</th>
                    <th>Status</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    <%
                    live l = new live();
                    List<live> list = l.getTender();
                    Iterator itr = list.iterator();
                    int i=0;
                    while(itr.hasNext())
                    {
                        l = (live)itr.next();
                        i++;
                    %>
                    <tr><td><%=i%></td>
                        <td><%=l.getLiveName()%></td>
                        <td><%=l.getLiveDetails()%></td>
                        <td><% if(l.getLtStatus()==0)
                            {%>
                            <span class="btn-sm btn-warning">To Bid</span>
                        <%  }else{
                            %>
                                <span class="btn-sm btn-warning">Tender Given</span>
                            <%
                            }
                            %>
                        </td>
                        <td>
                            <% if(l.getLtStatus()==0)
                            {%>
                            <a href="bid_view.jsp?id=<%=l.getLiveId() %>" class="btn-sm btn-info">View</a>
                            <%
                            }else{
                            %>
                            <p>Tender Clossed</p>
                            <% } %>
                        </td>
                    </tr>
                    <%
                    }
                    %>
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>
 <%}%>