<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="includes/header_admin.jsp" flush="true"></jsp:include>
 <div class="content-wrapper">
    <div class="container-fluid">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tender Reports</li>
      </ol>
      <div class="row card-body">
        <div class="col-sm-8">
            <form name="filter" action="filterTender" method="post" class="form-inline">
                <label for="from">Filter :&emsp;</label>
                <input name="from" id="from" class="form-control datep" required="" placeholder="From">&emsp;
                <input name="to" id="to" class="form-control datep" required="" placeholder="To">&emsp;
                <button type="submit" class="btn-sm btn-info" id="filter">Filter</button>
            </form>
            <br>
        </div>
        <div class="col-sm-12">
            <button type="button" class="btn btn-danger" onclick="save('Tender Report')" style="float: right">Download</button>
            <br>
            <table class="table table-bordered">
                <thead>
                <th style="width: 70px">Sl No</th>
                    <th style="width: 70px">Tender</th>
                    <th style="width: 70px">Category</th>
                    <th style="width: 70px">Post date</th>
                    <th style="width: 70px">Live Date</th>
                    <th style="width: 70px">Last date</th>
                    <th style="width: 70px">Status</th>
                    <th style="width: 70px">Actions</th>
                </thead>
                <tbody>
                    <%
                    tender t = new tender();
                    List<tender> list = t.getTendersAll();
                    Iterator itr = list.iterator();
                    int i=0;
                    String v1= "";
                    String v2="",v3="",v4="",v5="",v6="";
                    while(itr.hasNext())
                    {
                        t = (tender)itr.next();
                        v1 = v1 + t.getTenderName()+',';
                        v2 = v2 + t.getCategory()+',';
                        v3 = v3 + t.getPostDate()+',';
                        v4 = v4 + t.getLiveDate()+',';
                        v5 = v5 + t.getLastDate()+',';
                        if(t.getTstatus()==1){
                            v6=v6+"Active"+",";
                            }else if(t.getTstatus()==0){
                                v6=v6+"Inactive"+",";
                            }else if(t.getTstatus()==3){
                                v6=v6+"Clossed"+",";
                            }else{
                                v6=v6+"Suspended"+",";
                            }
                        i++;
                    %>
                    <tr><td><%=i%></td>
                        <td><%=t.getTenderName()%></td>
                        <td><%=t.getCategory() %></td>
                        <td><%=t.getPostDate() %></td>
                        <td><%=t.getLiveDate() %></td>
                        <td><%=t.getLastDate() %></td>
                        <td><% if(t.getTstatus()==1)
                            {%>
                                <span class="btn-sm btn-success">Active</span>
                            <%}else if(t.getTstatus()==0){
                            %>
                                <span class="btn-sm btn-danger">Inactive</span>
                            <%
                              }else if(t.getTstatus()==3){
                            %>
                                <span class="btn-sm btn-info">Clossed</span>
                            <% }else{
                            %>
                                <span class="btn-sm btn-warning">Suspended</span>
                            <%
                            }
                            %>
                        </td>
                        <td><a href="aview_tender.jsp?tid=<%=t.getTenderId()%>" class="btn-sm btn-info">View</a>&nbsp;</td>
                    </tr>
                    <%
                    }
                    %>
                </tbody>
            </table>
                <input type="hidden" id="tname" value="<%=v1 %>">
                <input type="hidden" id="category" value="<%=v2 %>">
                <input type="hidden" id="post" value="<%=v3 %>">
                <input type="hidden" id="live" value="<%=v4 %>">
                <input type="hidden" id="last" value="<%=v5 %>">
                <input type="hidden" id="status" value="<%=v6 %>">
            </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer_1.jsp" flush="true"></jsp:include>
 <%}%>