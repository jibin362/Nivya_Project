<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
  <link href="vendor/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Payment</div>
      <div class="card-body">
              <% float cost = (Float)session.getAttribute("cost");
                 int order_id = (Integer)session.getAttribute("order");
                 String name = (String)session.getAttribute("regname"); %>
          <form name="login" action="payment" method="post">
          <div class="form-group">
              <input class="form-control" id="name" name="name" type="text" placeholder="Enter Name" value="<%=name %>">
          </div>
          <div class="form-group">
                 <input class="form-control" id="cost" name="cost" disabled="" type="text" placeholder="Enter Cost" value="<%=cost%>">
                 <input type="hidden" name="amount" id="amount" value="<%=cost %>">
                 <input type="hidden" name="order" id="order" value="<%=order_id%>">
          </div>
          <div class="form-group">
            <input class="form-control" id="cardno" name="cardno" type="text" placeholder="Card No">
          </div>
          <div class="form-group">
              <input class="form-control" id="cvv" name="cvv" type="text" maxlength="3" placeholder="CVV">
          </div>
          <div class="form-group">
            <input class="form-control" id="expiry" name="expiry" type="text" placeholder="Expity Date">
          </div>
              <button type="submit" class="btn btn-primary btn-block" >Pay Now</button>
        </form>
      </div>
    </div>
      <div style="padding: 20px">
          <%
              if(request.getParameter("alert")!=null)
              {%>
              <p class="text-center text-warning">Profile Verification Failed!</p>   
             <% }
                else if(request.getParameter("error")!=null)
                {%>
                <p class="text-center text-danger">Invalid Login Credentials!</p>
              <%}
          %>
      </div>
  </div>
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="js/functions.js"></script>
</body>

</html>
