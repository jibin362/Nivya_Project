<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<jsp:include page="includes/header.jsp" flush="true"></jsp:include>
<div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#" title="Post">Home</a></li>
                        <li class="active">Add Tender</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div> 

<div class="content-wrapper">
    <div class="container">
      <div class="row card-body">
        <div class="col-sm-12">
            <form name="tender" action="Tender" method="post" class="">
                <div class="col-md-4 form-group">
                    <label for="category">Tender Category:&emsp;</label>
                    <select name="category" required="" id="category" class="form-control">
                        <option value="">Select</option>
                        <%
                tender t = new tender();
                List<tender> list = t.getCategorys();
                Iterator itr = list.iterator();
                while(itr.hasNext())
                {
                    tender data = (tender)itr.next();
                    %>
                    <option value="<%=data.getCategoryId()%>"><%=data.getCategory()%></option>
                    <%
                }
                %>
                    </select>
                </div>
                <div class="col-md-4 form-group">
                    <label for="tender_name">Tender Name:&emsp;</label>
                    <input type="text" name="tender_name" required="" id="tender_name" class="form-control" placeholder="Enter tender name">
                </div>
                <div class="col-md-4 form-group">
                    <label for="last_date">Tender Live on Date:&emsp;</label>
                    <input type="text" name="live_date" required="" id="live_date" class="form-control" placeholder="Enter live date">
                </div>
                <div class="col-md-4 form-group">
                    <label for="last_date">Submission Last Date:&emsp;</label>
                    <input type="text" name="last_date" required="" id="last_date" class="form-control" placeholder="Enter last date">
                </div>
                <div class="form-group col-md-12">
                    <label for="description">Tender Details</label>
                    <textarea name="description" rows="5" required="" id="description" class="form-control" placeholder="Enter tender details"></textarea>
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-info">Submit</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>
 <%
 }
 %>