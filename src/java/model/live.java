/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import connection.database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class live {
    private String live_name,live_date,live_details,username;
    private int user_id,live_id,lb_id,live_max_days,lb_status,lt_status;
    private float live_amount;
    
    public String getLiveName()
    {
        return live_name;
    }
    public String getLiveDate()
    {
        return live_date;
    }
    public String getLiveDetails()
    {
        return live_details;
    }
    public int getUserId()
    {
        return user_id;
    }
    public int getLiveId()
    {
        return live_id;
    }
    public int getLbId()
    {
        return lb_id;
    }
    public int getLiveMaxdays()
    {
        return live_max_days;
    }
    public int getLbStatus()
    {
        return lb_status;
    }
    public int getLtStatus()
    {
        return lt_status;
    }
    public float getLiveAmount()
    {
        return live_amount;
    }
    public String getUsername()
    {
        return username;
    }
    
    public void setLiveName(String live_name)
    {
        this.live_name=live_name;
    }
    public void setLiveDate(String live_date)
    {
        this.live_date=live_date;
    }
    public void setLiveDetails(String details)
    {
        this.live_details=details;
    }
    public void setUserId(int user_id)
    {
        this.user_id=user_id;
    }
    public void setLiveId(int live_id)
    {
        this.live_id=live_id;
    }
    public void setLbId(int lb_id)
    {
        this.lb_id=lb_id;
    }
    public void setLiveMaxdays(int max_days)
    {
        this.live_max_days=max_days;
    }
    public void setLbStatus(int lb_status)
    {
        this.lb_status=lb_status;
    }
    public void setLtStatus(int lt_status)
    {
        this.lt_status=lt_status;
    }
    public void setLiveAmount(float amount)
    {
        this.live_amount=amount;
    }
    public void setUsername(String username)
    {
        this.username=username;
    }
    
    public int addTender()
    {
        database db = new database();int i=0;
        Connection con = db.dbconnect();
        String s = "insert into live_tender(live_name,live_date,live_details,lt_status,user_id) values('"+live_name+"','"+live_date+"','"+live_details+"',"+lt_status+","+user_id+")";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
            } catch (SQLException ex) {
            Logger.getLogger(live.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public int addBid()
    {
        database db = new database();int i=0;
        Connection con = db.dbconnect();
        String s = "insert into live_bid(user_id,live_max_days,live_amount,lb_status) values("+user_id+","+live_max_days+","+live_amount+","+lb_status+")";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
                    } catch (SQLException ex) {
            Logger.getLogger(live.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public int upBid(int id)
    {
        database db = new database();int i=0;
        Connection con = db.dbconnect();
        String s ="update live_bid set lb_status="+lb_status+" where lb_id="+id;
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
                    } catch (SQLException ex) {
            Logger.getLogger(live.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public int upTender(int id)
    {
        database db = new database();int i=0;
        Connection con = db.dbconnect();
        String s ="update live_tender set lt_status="+lt_status+" where live_id="+id;
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
                    } catch (SQLException ex) {
            Logger.getLogger(live.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public List<live> getTender()
    {
        List<live> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s = "select * from live_tender";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                live l = new live();
                l.setLiveId(rs.getInt("live_id"));
                l.setLiveName(rs.getString("live_name"));
                l.setLiveDate(rs.getString("live_date"));
                l.setLiveDetails(rs.getString("live_details"));
                l.setLtStatus(rs.getInt("lt_status"));
                list.add(l);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(live.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<live> getTenderBids()
    {
        List<live> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String date = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
        String s = "select * from live_tender where live_date='"+date+"'";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                live l = new live();
                l.setLiveId(rs.getInt("live_id"));
                l.setLiveName(rs.getString("live_name"));
                l.setLiveDate(rs.getString("live_date"));
                l.setLiveDetails(rs.getString("live_details"));
                l.setLtStatus(rs.getInt("lt_status"));
                list.add(l);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(live.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<live> getBid()
    {
        database db = new database();
        List<live> list = new ArrayList();
        Connection con = db.dbconnect();
        String s = "select * from live_bid inner join users on live_bid.user_id=users.user_id";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                live l = new live();
                l.setLbId(rs.getInt("lb_id"));
                l.setLiveAmount(rs.getFloat("live_amount"));
                l.setLiveMaxdays(rs.getInt("live_max_days"));
                l.setLbStatus(rs.getInt("lb_status"));
                l.setUsername(rs.getString("company_name"));
                list.add(l);
            }
            con.close();
                    } catch (SQLException ex) {
            Logger.getLogger(live.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
