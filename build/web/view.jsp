<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
    int id = Integer.parseInt(request.getParameter("tid"));
    tender t = new tender();
    List<tender> list = t.getBidTender(id);
    Iterator itr = list.iterator();
    while(itr.hasNext())
    {
        t = (tender)itr.next();
%>
<jsp:include page="includes/header_supplier.jsp" flush="true"></jsp:include>
<div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#" title="Post">Home</a></li>
                        <li class="active">View Details</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div> 
<div class="content-wrapper">
    <div class="container">
      <div class="card-body">
          <div class="row" style="margin-left:-100px">
            <div class="col-sm-4 text-center">
                <h4 class="text-info">Company Name</h4>
                <p><%=t.getCompanyName() %></p>
            </div>
            <div class="col-sm-4 text-center">
                <h4 class="text-info">Tender Name</h4>
                <p><%=t.getTenderName() %></p>
            </div>
            <div class="col-sm-4 text-center">
                <h4 class="text-info">Contact Person</h4>
                <p><%=t.getOwnerName() %></p>
            </div>
            <div class="col-sm-4 text-center">
                <h4 class="text-info">Category Name</h4>
                <p><%=t.getCategory() %></p>
            </div>
            <div class="col-sm-4 text-center">
                <h4 class="text-info">Live Date</h4>
                <p><%=t.getLiveDate() %></p>
            </div>
            <div class="col-sm-4 text-center">
                <h4 class="text-info">Last Date</h4>
                <p><%=t.getLastDate() %></p>
            </div>
            <div class="col-sm-12 description">
                <h4 class="text-info">Description</h4>
                <p><%=t.getTenderDescription() %></p>
            </div>
            <div class="col-sm-4 description">
                <a href="bidon.jsp?id=<%=t.getTenderId()%>" class="btn btn-info bid">Bid</a>
            </div>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>
 <%
 }
}
 %>