/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import connection.database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bozz
 */
public class bid {
    private String details,bid_date,tender_name;
    private int user_id,tender_id,bid_id,amount,bid_status,work_days;
    
    public int getUserId()
    {
        return user_id;
    }
    public int getTenderId()
    {
        return tender_id;
    }
    public int getBidId()
    {
        return bid_id;
    }
    public int getAmount()
    {
        return amount;
    }
    public int getBidStatus()
    {
        return bid_status;
    }
    public String getBidDetails()
    {
        return details;
    }
    public String getBidDate()
    {
        return bid_date;
    }
    public String getTenderName()
    {
        return tender_name;
    }
    public int getWorkDays()
    {
        return work_days;
    }
    
    public void setUserId(int user_id)
    {
        this.user_id=user_id;
    }
    public void setTenderId(int tender_id)
    {
        this.tender_id=tender_id;
    }
    public void setBidId(int bid_id)
    {
        this.bid_id=bid_id;
    }
    public void setAmount(int amount)
    {
        this.amount=amount;
    }
    public void setBidStatus(int bid_status)
    {
        this.bid_status=bid_status;
    }
    public void setBidDetails(String details)
    {
        this.details=details;
    }
    public void setBidDate(String bid_date)
    {
        this.bid_date=bid_date;
    }
    public void setTenderName(String tender_name)
    {
        this.tender_name=tender_name;
    }
    public void setWorkDays(int work_days)
    {
        this.work_days=work_days;
    }
    
    public int addBid()
    {
        int i =0;
        database db = new database();
        Connection con = db.dbconnect();
        String s = "insert into bids(tender_id,user_id,amount,bid_date,bid_status,details,work_days) values("+tender_id+","+user_id+","+amount+",'"+bid_date+"',"+bid_status+",'"+details+"',"+work_days+")";
      //  System.out.println(s);
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(bid.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public List<bid> getBids(int id)
    {
        ResultSet rs;
        List<bid> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s ="select * from bids inner join tender on bids.tender_id=tender.tender_id where bids.user_id ="+id;
        //System.out.print(s);
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                bid b = new bid();
                b.setBidId(rs.getInt("bid_id"));
                b.setTenderId(rs.getInt("tender_id"));
                b.setTenderName(rs.getString("tender_name"));
                b.setAmount(rs.getInt("amount"));
                b.setBidDate(rs.getString("bid_date"));
                b.setBidStatus(rs.getInt("bid_status"));
                b.setWorkDays(rs.getInt("work_days"));
                list.add(b);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<bid> getBidsAll()
    {
        ResultSet rs;
        List<bid> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s ="select * from bids inner join tender on bids.tender_id=tender.tender_id";
        //System.out.print(s);
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                bid b = new bid();
                b.setBidId(rs.getInt("bid_id"));
                b.setTenderId(rs.getInt("tender_id"));
                b.setTenderName(rs.getString("tender_name"));
                b.setAmount(rs.getInt("amount"));
                b.setBidDate(rs.getString("bid_date"));
                b.setBidStatus(rs.getInt("bid_status"));
                b.setWorkDays(rs.getInt("work_days"));
                list.add(b);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<bid> getBidsBank(int user_id)
    {
        ResultSet rs;
        List<bid> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s ="select * from bids inner join tender on bids.tender_id=tender.tender_id where tender.user_id ="+user_id;
        //System.out.print(s);
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                bid b = new bid();
                b.setBidId(rs.getInt("bid_id"));
                b.setTenderId(rs.getInt("tender_id"));
                b.setTenderName(rs.getString("tender_name"));
                b.setAmount(rs.getInt("amount"));
                b.setBidDate(rs.getString("bid_date"));
                b.setBidStatus(rs.getInt("bid_status"));
                b.setBidDetails(rs.getString("details"));
                b.setWorkDays(rs.getInt("work_days"));
                list.add(b);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public bid getBid(int bid)
    {
        ResultSet rs;
        bid b = new bid();
        database db = new database();
        Connection con = db.dbconnect();
        String s ="select * from bids where bid_id ="+bid;
        //System.out.print(s);
        try {
            PreparedStatement ps = con.prepareStatement(s);
            rs = ps.executeQuery();
            while(rs.next())
            {
                
                b.setBidId(rs.getInt("bid_id"));
                b.setTenderId(rs.getInt("tender_id"));
                b.setAmount(rs.getInt("amount"));
                b.setBidDetails(rs.getString("details"));
                b.setBidStatus(rs.getInt("bid_status"));
                b.setWorkDays(rs.getInt("work_days"));
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(tender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return b;
    }
    
    public int editBid(int bid)
    {
        int i =0;
        database db = new database();
        Connection con = db.dbconnect();
        String s ="update bids set tender_id ="+tender_id+" ,user_id = "+user_id+", bid_status = 3,amount="+amount+",bid_date='"+bid_date+"',details='"+details+"',work_days ="+work_days+" where bid_id="+bid;
      //  System.out.println(s);
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(bid.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public int deleteBid(int bid)
    {
        int i =0;
        database db = new database();
        Connection con = db.dbconnect();
        String s = "delete from bids where bid_id="+bid;
      //  System.out.println(s);
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(bid.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public int editStatusBid(int bid)
    {
        int i =0;
        database db = new database();
        Connection con = db.dbconnect();
        String s = "update bids set bid_status="+bid_status+" where bid_id="+bid;
      //  System.out.println(s);
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(bid.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
}
