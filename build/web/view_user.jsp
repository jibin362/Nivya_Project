<%@page import="model.Users"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<jsp:include page="includes/header_admin.jsp" flush="true"></jsp:include>
 <div class="content-wrapper">
    <div class="container-fluid">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">View User</li>
      </ol>
      <div class="row card-body">
        <div class="col-sm-12">
                <% 
                    Users u = new Users();
                    int id = Integer.parseInt(request.getParameter("id"));
                    List<Users> list = u.getUsers(id);
                    Iterator itr = list.iterator();
                    int i=0;
                    while(itr.hasNext())
                    {
                        u = (Users)itr.next();
                %>
                <div class="col-md-4 form-group">
                    <label for="category">Company Name :&emsp;</label>
                    <span><strong><%=u.getCompany() %></strong></span>
                </div>
                <div class="col-md-4 form-group">
                    <label for="tender_name">Owner Name :&emsp;</label>
                    <span><strong><%=u.getOwnerName() %></strong></span>
                </div>
                <div class="col-md-4 form-group">
                    <label for="last_date">Reg No :&emsp;</label>
                    <span><strong><%=u.getRegno() %></strong></span>
                </div>
                <div class="col-md-4 form-group">
                    <label for="last_date">Email :&emsp;</label>
                    <span><strong><%=u.getEmail() %></strong></span>
                </div>
                <div class="col-md-4 form-group">
                    <label for="last_date">Phone :&emsp;</label>
                    <span><strong><%=u.getPhone() %></strong></span>
                </div>
                <div class="col-md-4 form-group">
                    <label for="last_date">Address :&emsp;</label>
                    <span><strong><%=u.getAddress() %></strong></span>
                </div>
                <div class="form-group col-md-4">
                    <label for="status">Status :&emsp;</label>
                    <% if(u.getStatus()==1)
                            {%>
                                <span class="btn-sm btn-success">Active</span>
                            <%}else if(u.getStatus()==2){
                            %>
                                <span class="btn-sm btn-danger">Inactive</span>
                            <%
                              }else{
                            %>
                                <span class="btn-sm btn-warning">Suspended</span>
                            <%
                            }
                            %>
                </div>
                <div class="col-md-4 formm-group">
                    <a href="#" class="btn btn-success uaccept" data-id="<%=u.getUserId() %>">Accept</a>&nbsp;
                    <a href="#" class="btn btn-danger ureject" data-id="<%=u.getUserId() %>" >Reject</a>
                </div>
            <% } %>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer_1.jsp" flush="true"></jsp:include>
 <%
 }
 %>