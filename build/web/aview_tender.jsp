<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<jsp:include page="includes/header_admin.jsp" flush="true"></jsp:include>
 <div class="content-wrapper">
    <div class="container-fluid">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Edit Tender</li>
      </ol>
      <div class="row card-body">
        <div class="col-sm-12">
                <% 
                    int id = Integer.parseInt(request.getParameter("tid"));
                    tender t = new tender();
                    List<tender> list = t.viewTender(id);
                    Iterator itr = list.iterator();
                    while(itr.hasNext())
                    {
                        t = (tender)itr.next();
                %>
                <div class="col-md-4 form-group">
                    <label for="category">Tender Category :&emsp;</label>
                    <span><strong><%=t.getCategory() %></strong></span>
                </div>
                    <input type="hidden" name="tid" id="tid" value="">
                <div class="col-md-4 form-group">
                    <label for="tender_name">Tender Name :&emsp;</label>
                    <span><strong><%=t.getTenderName() %></strong></span>
                </div>
                <div class="col-md-4 form-group">
                    <label for="last_date">Tender Live on Date :&emsp;</label>
                    <span><strong><%=t.getLiveDate() %></strong></span>
                </div>
                <div class="col-md-4 form-group">
                    <label for="last_date">Submission Last Date :&emsp;</label>
                    <span><strong><%=t.getLastDate() %></strong></span>
                </div>
                <div class="form-group col-md-4">
                    <label for="status">Status :&emsp;</label>
                    <% if(t.getTstatus()==1)
                            {%>
                                <span class="btn-sm btn-success">Active</span>
                            <%}else if(t.getTstatus()==0){
                            %>
                                <span class="btn-sm btn-danger">Inactive</span>
                            <%
                              }else if(t.getTstatus()==3){
                            %>
                                <span class="btn-sm btn-info">Clossed</span>
                            <% } else{
                            %>
                                <span class="btn-sm btn-warning">Suspended</span>
                            <%
                            }
                            %>
                </div>
                <div class="form-group col-md-12">
                    <label for="description">Tender Details :&emsp;</label>
                    <span><strong><%=t.getTenderDescription() %></strong></span>
                </div>
                <div class="col-md-4 formm-group">
                    <% if(t.getTstatus()!=3){%>
                    <button class="btn btn-warning" id="suspend" data-id="<%=t.getTenderId()%>">Suspend</button>
                    <button class="btn btn-success" id="active" data-id="<%=t.getTenderId()%>">Activate</button>
                    <% } %>
                </div>
            <% } %>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer_1.jsp" flush="true"></jsp:include>
 <%
 }
 %>