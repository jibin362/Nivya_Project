<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<jsp:include page="includes/header.jsp" flush="true"></jsp:include>
<div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#" title="Post">Home</a></li>
                        <li class="active">Edit Tender</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div> 

<div class="content-wrapper">
    <div class="container">
      <div class="row card-body">
        <div class="col-sm-12">
            <form name="tender" action="editTender" method="post" class="">
                <div class="col-md-4 form-group">
                    <label for="category">Tender Category:&emsp;</label>
                    <select name="category" required="" id="category" class="form-control">
                        <option value="">Select</option>
                        <%
                            int tid=0,category_id=0,status=0;String tender_name=null,last_date=null,live_date=null,description=null;
                            int id = Integer.parseInt(request.getParameter("tid"));
                tender t = new tender();
                List<tender> list = t.getCategorys();
                List<tender> output = t.editTender(id);
                Iterator itr2 = output.iterator();
                while(itr2.hasNext())
                {
                   tender t1 = (tender)itr2.next();
                   tid = t1.getTenderId();
                   category_id=t1.getTenderCategory();
                   tender_name=t1.getTenderName();
                   last_date=t1.getLastDate();
                   live_date=t1.getLiveDate();
                   description=t1.getTenderDescription();
                   status = t1.getTstatus();
                }
                Iterator itr = list.iterator();
                while(itr.hasNext())
                {
                    tender data = (tender)itr.next();
                    %>
                    <option value="<%=data.getCategoryId()%>" <% if(data.getCategoryId()== category_id){%>selected="selected"<%} %>><%=data.getCategory()%></option>
                    <%
                }
                %>
                    </select>
                </div>
                    <input type="hidden" name="tid" id="tid" value="<%=tid%>">
                <div class="col-md-4 form-group">
                    <label for="tender_name">Tender Name:&emsp;</label>
                    <input type="text" name="tender_name" required="" id="tender_name" class="form-control" placeholder="Enter tender name" value="<%=tender_name %>">
                </div>
                <div class="col-md-4 form-group">
                    <label for="last_date">Tender Live on Date:&emsp;</label>
                    <input type="text" name="live_date" required="" id="live_date" class="form-control" placeholder="Enter live date" value="<%=live_date %>">
                </div>
                <div class="col-md-4 form-group">
                    <label for="last_date">Submission Last Date:&emsp;</label>
                    <input type="text" name="last_date" required="" id="last_date" class="form-control" placeholder="Enter last date" value="<%=last_date %>">
                </div>
                <div class="form-group col-md-4">
                    <label for="status">Status</label>
                    <select name="status" required="" id="status" class="form-control">
                        <option value="0" <% if(status==0){%>selected="selected"<% } %>>Inactive</option>
                        <option value="1" <% if(status==1){%>selected="selected"<% } %>>Active</option>
                        <option value="2" <% if(status==2){%>selected="selected"<% } %>>Suspend</option>
                    </select>
                </div>
                <div class="form-group col-md-12">
                    <label for="description">Tender Details</label>
                    <textarea name="description" rows="5" required="" id="description" class="form-control" placeholder="Enter tender details"><%=description %></textarea>
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-info">Update</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer.jsp" flush="true"></jsp:include>
 <%
 }
 %>