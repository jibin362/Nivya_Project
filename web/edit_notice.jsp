<%@page import="model.notice"%>
<%@page import="java.util.List"%>
<%@page import="model.tender"%>
<%@page import="java.util.Iterator"%>
<% if(session.getAttribute("user_id") == null )
    {
        response.sendRedirect("login.jsp");
    }
else{
%>
<jsp:include page="includes/header_admin.jsp" flush="true"></jsp:include>
 <div class="content-wrapper">
    <div class="container-fluid">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Add Notice</li>
      </ol>
      <div class="row card-body">
        <div class="col-sm-12">
            <form name="tender" action="editNotice" method="post" class="">
                <%
                notice n = new notice();
                int id = Integer.parseInt(request.getParameter("tid"));
                List<notice> list = n.getNotice(id);
                Iterator itr = list.iterator();
                while(itr.hasNext()){
                    n = (notice)itr.next();
                %>
                <div class="col-md-4 form-group">
                    <label for="tender_name">Notice Name:&emsp;</label>
                    <input type="text" name="name" required="" id="name" class="form-control" value="<%=n.getName() %>" placeholder="Enter notice name">
                    <input type="hidden" name="id" id="id" value="<%=id%>">
                </div>
                <div class="form-group col-md-12">
                    <label for="description">Notice Details</label>
                    <textarea name="details" rows="5" required="" id="details" class="form-control" placeholder="Enter notice details"><%=n.getDetails() %></textarea>
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-info">Submit</button>
                </div>
                <% } %>
            </form>
        </div>
      </div>
    </div>
  </div>
 <jsp:include page="includes/footer_1.jsp" flush="true"></jsp:include>
 <%
 }
 %>