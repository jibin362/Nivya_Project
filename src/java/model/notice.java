/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import connection.database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class notice {
    private String name, details, date;
    private int n_id;
    
    public int getNid()
    {
        return n_id;
    }
    public String getName()
    {
        return name;
    }
    public String getDetails()
    {
        return details;
    }
    public String getDate()
    {
        return date;
    }
    public void setNid(int n_id)
    {
        this.n_id=n_id;
    }
    public void setName(String name)
    {
        this.name=name;
    }
    public void setDetails(String details)
    {
        this.details=details;
    }
    public void setDate(String date)
    {
        this.date=date;
    }
    
    public int addNotice()
    {
        int i = 0;
        database db = new database();
        Connection con = db.dbconnect();
        String s = "insert into notice(name,details,date) values('"+name+"','"+details+"','"+date+"')";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(notice.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public List<notice> allNotice()
    {
        List<notice> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s = "select * from notice order by date desc";
        try {
            PreparedStatement ps = con.prepareStatement(s);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                notice n = new notice();
                n.setNid(rs.getInt("n_id"));
                n.setName(rs.getString("name"));
                n.setDetails(rs.getString("details"));
                n.setDate(rs.getString("date"));
                list.add(n);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(notice.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<notice> getNotice(int id)
    {
        List<notice> list = new ArrayList();
        database db = new database();
        Connection con = db.dbconnect();
        String s = "select * from notice where n_id="+id;
        try {
            PreparedStatement ps = con.prepareStatement(s);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                notice n = new notice();
                n.setNid(rs.getInt("n_id"));
                n.setName(rs.getString("name"));
                n.setDetails(rs.getString("details"));
                n.setDate(rs.getString("date"));
                list.add(n);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(notice.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public int updateNotice(int id)
    {
        int i = 0;
        database db = new database();
        Connection con = db.dbconnect();
        String s = "update notice set name ='"+name+"', details ='"+details+"', date='"+date+"' where n_id="+id;
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(notice.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    public int deleteNotice(int id)
    {
        int i = 0;
        database db = new database();
        Connection con = db.dbconnect();
        String s = "delete from notice where n_id="+id;
        try {
            PreparedStatement ps = con.prepareStatement(s);
            i = ps.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(notice.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
}
